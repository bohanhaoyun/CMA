package priv.scau.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import priv.scau.dao.CourseMapper;
import priv.scau.en.Course;
import priv.scau.en.Coursedocument;
import priv.scau.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService{

	@Autowired
	CourseMapper cMapper;
	
	@Override
	public List<Course> selectAllCourse(Course course) {
		// TODO 自动生成的方法存根
		return cMapper.selectAllCourse(course);
	}

	@Override
	public Course getCourseByName(String courseName) {
		// TODO Auto-generated method stub
		return cMapper.getCourseByName(courseName);
	}

	
}
