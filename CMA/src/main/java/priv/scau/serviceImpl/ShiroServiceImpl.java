package priv.scau.serviceImpl;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import priv.scau.dao.RoleMapper;
import priv.scau.en.Role;
import priv.scau.en.Teacher;
import priv.scau.service.ShiroService;
@Service
public class ShiroServiceImpl implements ShiroService{

	@Autowired
	RoleMapper roleMapper;
	
	@Override
	public String getRoleByTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		Role role = new Role();
		role = roleMapper.getRoleByTeacher(teacher);
		return role.getRole();
	}

	@Override
	public Map<String, Object> getUserMsg() {
		Map<String, Object> result = new HashMap<String, Object>();
		Session session = SecurityUtils.getSubject().getSession();
		Teacher teacher = (Teacher) session.getAttribute("gray");
		result.put("user", teacher);
		result.put("role", roleMapper.getRoleByTeacher(teacher));
		return result;
	}

}
