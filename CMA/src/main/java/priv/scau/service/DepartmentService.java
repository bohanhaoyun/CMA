package priv.scau.service;

import java.util.List;

import priv.scau.en.Department;

public interface DepartmentService {

	List<Department> getDepartmentByAcademe(Department de);

	void addDepartment(Department dto);

	void delDepartment(Department dto);

	List<Department> getDepartByUser(Department de);

}
