package priv.scau.en;

import javax.persistence.Transient;

public class Teacher {
    private Integer teacherid;

    private String workid;

    private String name;

    private String password;

    private String academe;
    
    private String imgUrl; 
    
    @Transient
    private String academeName;
    
    @Transient
    private String roleName; 
    

    public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getAcademeName() {
		return academeName;
	}

	public void setAcademeName(String academeName) {
		this.academeName = academeName;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    public String getWorkid() {
        return workid;
    }

    public void setWorkid(String workid) {
        this.workid = workid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAcademe() {
        return academe;
    }

    public void setAcademe(String academe) {
        this.academe = academe;
    }
}