package priv.scau.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import priv.scau.en.Coursedocument;
import priv.scau.en.Teacher;
import priv.scau.service.ShiroService;
import priv.scau.service.TeacherService;

@Controller
public class TeacherController {

	@Autowired
	TeacherService tService;
	@Autowired
	ShiroService shiroService;
	//获取所有教师信息
	@RequestMapping(value="/teacher/getAll",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> selectAllTeacher(){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		resultMap.put("data",tService.selectAllTeacher(teacher));
		resultMap.put("msg","success");
		return resultMap;
	}
	
	//通过id获取教师信息
	@RequestMapping(value="/teacher/getTeacherById",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getTeacherById(){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		resultMap.put("data",tService.getTeacherById(teacher));
		resultMap.put("msg","success");
		return resultMap;
	}
	
	//查询获得教师表格信息
	@RequestMapping(value="/teacher/getTeacherGrid",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getTeacherGrid(Integer page,Integer limit,String key,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		PageHelper.startPage(page, limit);
		List<Teacher> list = new ArrayList<Teacher>();
		if(key != null && "add".equals(key)) {
			//增加空白列
			key = null;
			list = tService.getTeacherGrid(key);
			Teacher t = new Teacher();
			list.add(0, t);
		}else {
			list = tService.getTeacherGrid(key);
		}
		PageInfo<Teacher> pageInfo = new PageInfo<Teacher>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", 0);
		return resultMap;
	}
	
	//添加教师
	@RequestMapping(value="/teacher/addTeacher",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addTeacher(Teacher teacher){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		if(teacher.getTeacherid()!=null) {
			resultMap.put("result", tService.updateTeacher(teacher));	
			
		}else {
			resultMap.put("result", tService.addTeacher(teacher));			
		}
		resultMap.put("msg","success");
		return resultMap;
	}
	
	//删除教师信息
	@RequestMapping(value="/teacher/delTeacher",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delTeacher(Teacher teacher){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		tService.delTeacher(teacher);
		resultMap.put("msg","success");
		return resultMap;
	}
	
	//通过excel导入教师信息
	@RequestMapping(value="/teacher/uploadTeacher",method=RequestMethod.POST)  
    @ResponseBody  
    public Map<String, Object> upload(MultipartFile file,HttpServletRequest request){  
		Map<String, Object> resultMap = new HashMap<String ,Object>();
        String fileName = file.getOriginalFilename();    
        //System.out.println(fileName);
        XSSFWorkbook workBook = null;
		try {
			InputStream input = file.getInputStream();  
			workBook = new XSSFWorkbook(input);
		} catch (IOException e) {
			resultMap.put("result", "success");
			resultMap.put("msg", "文件解析失败");
			e.printStackTrace();
			return resultMap;
		}  
        XSSFSheet sheet = workBook.getSheetAt(0);
        for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
        	Teacher teacher = new Teacher();
        	XSSFRow row = sheet.getRow(i);
        	teacher.setName(row.getCell(0).toString());
        	teacher.setWorkid(row.getCell(1).toString());
        	teacher.setAcademe(row.getCell(2).toString());
        	teacher.setPassword(row.getCell(3).toString());
        	teacher.setRoleName(row.getCell(4).toString());
        	tService.addTeacher(teacher);
        }
        resultMap.put("result", "success");
        resultMap.put("msg", "文件上传成功");
        return resultMap;  
    }  
	
	//修改头像
	@RequestMapping(value="/teacher/uploadImg",method=RequestMethod.POST)  
    @ResponseBody  
    public Map<String, Object> uploadImg(MultipartFile file,HttpServletRequest request){  
		Map<String, Object> resultMap = new HashMap<String ,Object>();
        String path = request.getSession().getServletContext().getRealPath("uploadImg");
        //System.out.println(path);
        //String fileName = file.getOriginalFilename();    
        
        String fileName = "D:\\git\\CMA\\src\\main\\webapp\\uploadImg";
        System.out.println(fileName);
        Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
        File dir = new File(path,teacher.getWorkid()+".png");          
        if(!dir.exists()){  
            dir.mkdirs();  
        }  
        //MultipartFile自带的解析方法  
        try {
			file.transferTo(dir);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}  
        return resultMap;
	}
    
    @RequestMapping(value="/teacher/modifyPassword",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> modifyPassword(int id,String oldPassword,String newPassword){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Teacher teacher = new Teacher();
		teacher.setTeacherid(id);
		teacher.setPassword(oldPassword);
		if(tService.checkPassword(teacher)) {
			teacher.setPassword(newPassword);
			tService.modifyPassword(teacher);
			resultMap.put("msg","修改成功");
		}else {
			resultMap.put("msg","原密码错误，请重新输入");
		}
		return resultMap;
	}
    
}
