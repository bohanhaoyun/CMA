package priv.scau.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import priv.scau.en.Coursedocument;

@Service
public interface CoursedocumentService {
	List<Coursedocument> getDocument(String key, Integer page, Integer limit);

	void addNewDocument(Coursedocument dto);

	List<Coursedocument> getDocumentByWorkId(Coursedocument course);

	void changeStatus(Coursedocument document);

	List<Coursedocument> getDocumentApproval(Coursedocument course);

	List<Coursedocument> getDocumentBySearch(Coursedocument document, Integer page, Integer limit);

	List<Coursedocument> getCountByCourse(Coursedocument document);

	Map<String, Object> getAcademeDocument(Coursedocument document);

	List<Coursedocument> getPeriod();

	List<Coursedocument> getAlSubmitDocument(Coursedocument course);

	List<Coursedocument> getDocumentByDepartment(String period, String department, Integer page, Integer limit);

	List<Coursedocument> getDocumentByAcademe(String academeName, String status, Integer page, Integer limit);

	String batchInsert(List<Coursedocument> list);
}
