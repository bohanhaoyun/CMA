package priv.scau.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import priv.scau.dao.RoleMapper;
import priv.scau.dao.RoleUserMapper;
import priv.scau.dao.TeacherMapper;
import priv.scau.en.Academe;
import priv.scau.en.Coursedocument;
import priv.scau.en.Role;
import priv.scau.en.RoleUser;
import priv.scau.en.Teacher;
import priv.scau.service.AcademeService;
import priv.scau.service.TeacherService;

@Service
public class TeacherServiceImpl implements TeacherService{

	@Autowired
	TeacherMapper tMapper;
	
	@Autowired
	AcademeService academeService;
	
	@Autowired
	RoleMapper roleMapper;
	
	@Autowired
	RoleUserMapper ruMapper;
	
	@Override
	public Object getTeacherById(Teacher teacher) {
		// TODO 自动生成的方法存根
		return tMapper.getTeacherByWorKId(teacher);
	}

	@Override
	public List<Teacher> getTeacherGrid(String key) {
		// TODO 自动生成的方法存根
		//System.out.println(key);
		List<Teacher> list = new ArrayList<Teacher>();
		list = tMapper.getTeacherGrid(key);
		for(Teacher t:list) {
			t.setRoleName(roleMapper.getRoleByTeacher(t).getDescription());
		}
		return list;
	}

	@Override
	public String addTeacher(Teacher teacher) {
		// TODO 自动生成的方法存根
		//根据学院名称获取学院id
		Academe adademe = new Academe();
		adademe = academeService.getAcademeByName(teacher.getAcademeName());
		Role role = new Role();
		role = roleMapper.getRoleByDescription(teacher.getRoleName());
		if(adademe==null) {
			return "学院信息错误";
		}else {
			if(role == null) {
				return "角色信息错误";
			}else {
				RoleUser roleUser = new RoleUser();
				roleUser.setRoleid(role.getId());
				roleUser.setUserid(teacher.getTeacherid());
				ruMapper.insertSelective(roleUser);
				teacher.setAcademe(adademe.getId().toString());
				tMapper.insertSelective(teacher);
				return "增加成功";
			}	
		}
		
	}

	@Override
	public String updateTeacher(Teacher teacher) {
		// TODO 自动生成的方法存根
		Academe adademe = new Academe();
		adademe = academeService.getAcademeByName(teacher.getAcademeName());
		Role role = new Role();
		role = roleMapper.getRoleByDescription(teacher.getRoleName());
		if(adademe==null) {
			return "学院信息错误";
		}else {
			if(role == null) {
				return "角色信息错误";
			}else {
				RoleUser roleUser = new RoleUser();
				roleUser = ruMapper.selectByUser(teacher.getTeacherid());
				roleUser.setRoleid(role.getId());
				roleUser.setUserid(teacher.getTeacherid());
				ruMapper.updateByPrimaryKeySelective(roleUser);
				teacher.setAcademe(adademe.getId().toString());
				tMapper.updateByPrimaryKeySelective(teacher);
				return "更新成功";
			}
		}
		
	}

	@Override
	public void delTeacher(Teacher teacher) {
		// TODO 自动生成的方法存根
		tMapper.deleteByPrimaryKey(teacher.getTeacherid());
	}

	@Override
	public boolean checkPassword(Teacher teacher) {
		// TODO Auto-generated method stub
		List<Teacher> list = new ArrayList<Teacher>();
		list = tMapper.checkPassword(teacher);
		if(list.size()!=0) {
			//密码正确返回true
			return true;
		}else {
			return false;
		}
	}

	@Override
	public void modifyPassword(Teacher teacher) {
		// TODO Auto-generated method stub
		tMapper.updateByPrimaryKeySelective(teacher);
	}

	@Override
	public Teacher getTeacherByWorKId(Teacher teacher) {
		return tMapper.getTeacherByWorKId(teacher);
	}

	@Override
	public List<Teacher> selectAllTeacher(Teacher teacher) {
		return tMapper.selectAllTeacher(teacher);
	}

	@Override
	public Teacher getTeacherByName(String teaherName) {
		// TODO Auto-generated method stub
		return tMapper.getTeacherByName(teaherName);
	}
	
}
