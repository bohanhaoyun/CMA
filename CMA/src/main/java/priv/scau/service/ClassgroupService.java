package priv.scau.service;

import java.util.List;

import org.springframework.stereotype.Service;

import priv.scau.en.Classgroup;
import priv.scau.en.Coursedocument;
@Service
public interface ClassgroupService {
	List<Classgroup> selectAllClass();

	String getCoursePerson(Coursedocument dto);

	Classgroup getClassByName(String className);
}
