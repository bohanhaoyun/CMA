package priv.scau.dao;

import priv.scau.en.Role;
import priv.scau.en.Teacher;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

	Role getRoleByTeacher(Teacher teacher);

	Role getRoleByDescription(String roleName);
}