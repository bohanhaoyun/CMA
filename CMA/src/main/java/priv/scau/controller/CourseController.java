package priv.scau.controller;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import priv.scau.en.Course;
import priv.scau.en.Teacher;
import priv.scau.service.CourseService;
import priv.scau.service.ShiroService;

@Controller
public class CourseController {
	
	@Autowired
	CourseService courseService;
	
	@Autowired
	ShiroService shiroService;
	
	/**
	 * 获取所有课程
	 * @return
	 */
	@RequestMapping(value="/course/getAllCourse",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> selectAllCourse(){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		Course course = new Course();
		course.setAcademe(teacher.getAcademe());
		resultMap.put("data",courseService.selectAllCourse(course));
		resultMap.put("msg","success");
		return resultMap;
	}
}
