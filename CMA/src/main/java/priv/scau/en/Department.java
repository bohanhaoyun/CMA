package priv.scau.en;

public class Department {
    private Integer id;

	private String department;

	private String academe;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getAcademe() {
		return academe;
	}

	public void setAcademe(String academe) {
		this.academe = academe;
	}
}