package priv.scau.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import priv.scau.service.ClassgroupService;
@Controller
public class ClassgroupController {

	@Autowired
	ClassgroupService cgservice;
	
	/**
	 * 获取所有班级
	 * @return
	 */
	@RequestMapping(value="/classGroup/getAllClass",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getAllClass(){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		resultMap.put("data",cgservice.selectAllClass());
		resultMap.put("msg","success");
		return resultMap;
	}
}
