package priv.scau.service;

import java.util.List;

import org.springframework.stereotype.Service;

import priv.scau.en.Course;
import priv.scau.en.Coursedocument;
@Service
public interface CourseService {
	List<Course> selectAllCourse(Course course);

	Course getCourseByName(String courseName);

}
