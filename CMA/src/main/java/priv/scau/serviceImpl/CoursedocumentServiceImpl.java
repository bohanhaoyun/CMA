package priv.scau.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import priv.scau.dao.CoursedocumentMapper;
import priv.scau.en.Classgroup;
import priv.scau.en.Course;
import priv.scau.en.Coursedocument;
import priv.scau.en.LineCharts;
import priv.scau.en.Teacher;
import priv.scau.service.ClassgroupService;
import priv.scau.service.CourseService;
import priv.scau.service.CoursedocumentService;
import priv.scau.service.ShiroService;
import priv.scau.service.TeacherService;
@Service
public class CoursedocumentServiceImpl implements CoursedocumentService{

	@Autowired
	CoursedocumentMapper docutmentMapper;
	
	@Autowired
	ShiroService shiroService;
	@Autowired
	CourseService courseService;
	
	@Autowired
	TeacherService teacherService;
	
	@Autowired
	ClassgroupService classService;
	
	@Override
	public List<Coursedocument> getDocument(String key, Integer page, Integer limit) {
		// TODO 自动生成的方法存根
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		Coursedocument doc = new Coursedocument();
		doc.setKey(key);
		doc.setTeacherid(teacher.getTeacherid());
		PageHelper.startPage(page, limit);
		return docutmentMapper.getAllDocuments(doc);
	}

	@Override
	public void addNewDocument(Coursedocument dto) {
		// TODO 自动生成的方法存根
		docutmentMapper.insertSelective(dto);
	}

	@Override
	public List<Coursedocument> getDocumentByWorkId(Coursedocument course) {
		// TODO 自动生成的方法存根
		return docutmentMapper.getDocumentByWorkId(course);
	}

	@Override
	public void changeStatus(Coursedocument document) {
		// TODO 自动生成的方法存根
		//System.out.println(document);
		docutmentMapper.changeStatus(document);
		if(document.getStatus().equals("审核中")) {
			Coursedocument doc = new Coursedocument();
			doc = docutmentMapper.selectByPrimaryKey(document.getId());
			doc.setReceived(doc.getReceivable());
			doc.setFinishdate(new Date());
			docutmentMapper.updateByPrimaryKeySelective(doc);
		}
	}

	@Override
	public List<Coursedocument> getDocumentApproval(Coursedocument course) {
		return docutmentMapper.getDocumentApproval(course);
	}

	
	@Override
	public List<Coursedocument> getDocumentBySearch(Coursedocument document, Integer page, Integer limit) {
		// TODO Auto-generated method stub
		if("".equals(document.getPeriod())) {
			document.setPeriod(null);
		}
		if("".equals(document.getTeacherid())|| document.getTeacherid()==null) {
			Map<String, Object> userMap = new HashMap<String,Object>();
			userMap = shiroService.getUserMsg();
			Teacher teacher = new Teacher();
			teacher = (Teacher) userMap.get("user");
			document.setTeacherid(teacher.getTeacherid());
			PageHelper.startPage(page, limit);
			return docutmentMapper.getDocumentBySearchSessionTeacher(document);	
		}else {
			PageHelper.startPage(page, limit);
			return docutmentMapper.getDocumentBySearch(document);			
		}
	}

	@Override
	public List<Coursedocument> getCountByCourse(Coursedocument document) {
		// TODO Auto-generated method stub
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		document.setTeacherid(teacher.getTeacherid());
		if("".equals(document.getCourseid())){
			document.setCourseid(null);
		}
		if("".equals(document.getPeriod())) {
			document.setPeriod(null);
		}
		System.out.println(document);
		return docutmentMapper.getCountByCourse(document);
	}

	@Override
	public Map<String, Object> getAcademeDocument(Coursedocument document) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<LineCharts> list = new ArrayList<LineCharts>();
		List<String> nameList = new ArrayList<String>();
		List<Integer> valueList = new ArrayList<Integer>();
		System.out.println(document.getStatus());
		list = docutmentMapper.getAcademeDocument(document);
		for(LineCharts chart: list){
			nameList.add(chart.getName());
			valueList.add(Integer.valueOf(chart.getValue()));
		}
		result.put("nameList", nameList);
		result.put("valueList", valueList);
		return result;
	}

	@Override
	public List<Coursedocument> getPeriod() {
		// TODO Auto-generated method stub
		return docutmentMapper.getPeried();
	}

	@Override
	public List<Coursedocument> getAlSubmitDocument(Coursedocument course) {
		// TODO Auto-generated method stub
		return docutmentMapper.getAlSubmitDocument(course);
	}

	@Override
	public List<Coursedocument> getDocumentByDepartment(String period, String department, Integer page, Integer limit) {
		Map<String ,Object> map = new HashMap<String ,Object>();
		if("".equals(department)) {
			map.put("department", null);
		}else {
			map.put("department", department);
		}
		if("".equals(period)) {
			map.put("period", null);
		}else {
			map.put("period", period);
		}
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		map.put("teacherId", teacher.getTeacherid());
		System.out.println(teacher.getTeacherid());
		PageHelper.startPage(page, limit);
		return docutmentMapper.getDocumentByDepartment(map);	
	}

	@Override
	public List<Coursedocument> getDocumentByAcademe(String academeName,String status, Integer page, Integer limit) {
		PageHelper.startPage(page, limit);
		Map<String ,Object> map = new HashMap<String ,Object>();
		map.put("academeName", academeName);
		map.put("status", status);
		return docutmentMapper.getDocumentByAcademe(map);
	}

	@Override
	public String batchInsert(List<Coursedocument> list) {
		for(int i = 0; i < list.size(); i ++) {
			Coursedocument doc = list.get(i);
			Course course = new Course();
			course = courseService.getCourseByName(doc.getCourseName());
			if(course == null) {
				return "第"+i+"行数据课程名称有误";
			}else {
				doc.setCourseid(course.getCourseid());
			}
			Teacher teacher = new Teacher();
			teacher = teacherService.getTeacherByName(doc.getTeaherName());
			if(teacher == null) {
				return "第"+i+"行教师名称有误";
			}else {
				doc.setTeacherid(teacher.getTeacherid());
			}
			Classgroup cla = new Classgroup();
			cla = classService.getClassByName(doc.getClassName());
			if(cla == null) {
				return "第"+i+"行班级名称有误";
			}else {
				doc.setClassid(cla.getId());
				doc.setReceivable(cla.getNumber().toString());
				doc.setStatus("未提交");
			}
			this.addNewDocument(doc);
		}
		return "导入成功";
	}
	
}
