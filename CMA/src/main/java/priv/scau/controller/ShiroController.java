package priv.scau.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import priv.scau.service.ShiroService;
import priv.scau.service.TeacherService;

@Controller
public class ShiroController {

	@Autowired
	TeacherService tService;
	@Autowired
	ShiroService shiroService;
	
	//获取所有教师信息
	@RequestMapping(value="/shiro/login",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> login(String account,String password){
		System.out.println("user:"+account+"====pass:"+password);
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		
		//从SecurityUtils里面创建一个subject  
		Subject subject = SecurityUtils.getSubject();  
        //在认证提交前，需要准备token(令牌)  
        UsernamePasswordToken token = new UsernamePasswordToken(account, password); 
        String result = "success";
        try {  
            //执行认证提交  
            subject.login(token);  
            System.out.println(subject.getPrincipal());
        } catch (UnknownAccountException e) {  
            result = "用户名不存在";
        }  catch (IncorrectCredentialsException e) {
			result = "密码不正确";
		}
        //是否认证通过  
        boolean isAuthenticated = subject.isAuthenticated();  
        System.out.println("是否认证通过：" + isAuthenticated);  
        System.out.println("结果：" + result);  
        resultMap.put("result", result);
		return resultMap;
	}
	
	@RequestMapping(value="/shiro/getUserMsg",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getUserMsg(){
		return shiroService.getUserMsg();
	}
}
