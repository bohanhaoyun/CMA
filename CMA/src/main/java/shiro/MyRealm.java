package shiro;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.SimpleAccount;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import priv.scau.dao.TeacherMapper;
import priv.scau.en.Teacher;
import priv.scau.service.ShiroService;
import priv.scau.service.TeacherService;
/**
 * 自定义Realm
 * @author CatScan
 *
 */
public class MyRealm extends AuthorizingRealm{
    final String realmName="customRealm";

    @Autowired
    TeacherService teacherService;
    
    @Autowired
    ShiroService shiroService;
    
    public static final String SESSION_USER_KEY = "gray";  
    //设置realmName
    @Override
    public void setName(String name) {
        super.setName(realmName);
    }



    //用于认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken token) throws AuthenticationException {
        // 第一步从token中取出用户发送过来的身份信息
        String userCode = (String) token.getPrincipal();
        String password = "";
        Teacher teacher = new Teacher();
        teacher.setWorkid(userCode);
        teacher = teacherService.getTeacherByWorKId(teacher);
        if(teacher!=null) {
        	password = teacher.getPassword();
        	Session session = SecurityUtils.getSubject().getSession();  
            session.setAttribute(MyRealm.SESSION_USER_KEY, teacher);   
        }else{
        	throw new UnknownAccountException("用户名有错");
        }

        AuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(userCode, password, this.getName());  
        /*SimpleAccount account = new SimpleAccount(userCode, pwd, this.getName());*/
        return simpleAuthenticationInfo;
    }



    //用于授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        //从principals获取主身份信息
        //将getPrimaryPrincipal()返回的值强制转换为真实身份信息【在上边的doGetAuthenticationInfo()认证通过填充到SimpleAuthenticationInfo中的身份信息】
        String userCode = (String) principals.getPrimaryPrincipal();
        System.out.println(userCode);
        System.out.println("获取权限");
        //session获取当前用户id
        Teacher teaher = new Teacher();
        teaher = (Teacher) SecurityUtils.getSubject().getSession().getAttribute(MyRealm.SESSION_USER_KEY);
        String role = shiroService.getRoleByTeacher(teaher); 
        System.out.println(role);
        
        //根据身份信息获取权限信息
        //先链接数据库。。。
        //模拟从数据库获取数据
        /*List<String> permissions = new ArrayList<String>();
        permissions.add("user:create");//用户的创建权限
        permissions.add("user:update");//用户的修改
        permissions.add("item:add");//商品的添加权限
*/        //....等等权限

        //查到权限数据，返回
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        
        simpleAuthorizationInfo.addRole(role);
        
        
        //将List里面的权限填充进去
       /* simpleAuthorizationInfo.addStringPermissions(permissions);*/

        return simpleAuthorizationInfo;

    }
    
}