package priv.scau.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import priv.scau.dao.ClassgroupMapper;
import priv.scau.en.Classgroup;
import priv.scau.en.Coursedocument;
import priv.scau.service.ClassgroupService;
@Service
public class ClassgroupServiceImpl implements ClassgroupService{

	@Autowired
	ClassgroupMapper cgMapper;
	
	@Override
	public List<Classgroup> selectAllClass() {
		// TODO 自动生成的方法存根
		return cgMapper.selectAllClass();
	}

	@Override
	public String getCoursePerson(Coursedocument dto) {
		Classgroup clas = new Classgroup();
		clas.setId(dto.getClassid());
		clas = cgMapper.getCoursePerson(clas);
		return clas.getNumber().toString();
	}

	@Override
	public Classgroup getClassByName(String className) {
		// TODO Auto-generated method stub
		return cgMapper.getClassByName(className);
	}

}
