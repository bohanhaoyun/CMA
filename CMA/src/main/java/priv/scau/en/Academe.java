package priv.scau.en;

public class Academe {
    private Integer id;

	private String academename;

	private String academecode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAcademename() {
		return academename;
	}

	public void setAcademename(String academename) {
		this.academename = academename;
	}

	public String getAcademecode() {
		return academecode;
	}

	public void setAcademecode(String academecode) {
		this.academecode = academecode;
	}

}