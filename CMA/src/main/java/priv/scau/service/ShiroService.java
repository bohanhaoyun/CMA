package priv.scau.service;

import java.util.Map;

import priv.scau.en.Teacher;

public interface ShiroService {
	public String getRoleByTeacher(Teacher teacher);

	public Map<String, Object> getUserMsg();
}
