package priv.scau.dao;

import java.util.List;

import priv.scau.en.Coursedocument;
import priv.scau.en.Teacher;

public interface TeacherMapper {
    int deleteByPrimaryKey(Integer teacherid);

    int insert(Teacher record);

    int insertSelective(Teacher record);

    Teacher selectByPrimaryKey(Integer teacherid);

    int updateByPrimaryKeySelective(Teacher record);

    int updateByPrimaryKey(Teacher record);
    
    List<Teacher> selectAllTeacher(Teacher teacher);

	List<Teacher> getTeacherGrid(String key);

	List<Teacher> checkPassword(Teacher teacher);

	Teacher getTeacherByWorKId(Teacher teacher);

	Teacher getTeacherByName(String teaherName);
}