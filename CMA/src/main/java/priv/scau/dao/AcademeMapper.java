package priv.scau.dao;

import java.util.List;

import priv.scau.en.Academe;

public interface AcademeMapper {
    int deleteByPrimaryKey(Integer id);

	int insert(Academe record);

	int insertSelective(Academe record);

	Academe selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Academe record);

	int updateByPrimaryKey(Academe record);

	List<Academe> getAcademeGrid(String key);

	List<Academe> checkCode(Academe academe);

	Academe getAcademeByName(String academeName);
}