package priv.scau.service;

import java.util.List;

import priv.scau.en.Academe;

public interface AcademeService {

	List<Academe> getAcademeGrid(String key);

	List<Academe> checkCode(Academe academe);

	void addAcademe(Academe academe);

	void updateAcademe(Academe academe);

	void delAcademe(Academe academe);

	Academe getAcademeByName(String academeName);

}
