package priv.scau.service;

import java.util.List;

import org.springframework.stereotype.Service;

import priv.scau.en.Coursedocument;
import priv.scau.en.Teacher;

@Service
public interface TeacherService {
	List<Teacher> selectAllTeacher(Teacher teacher);

	Object getTeacherById(Teacher teacher);

	List<Teacher> getTeacherGrid(String key);

	String addTeacher(Teacher teacher);

	String updateTeacher(Teacher teacher);

	void delTeacher(Teacher teacher);

	boolean checkPassword(Teacher teacher);

	void modifyPassword(Teacher teacher);
	
	Teacher getTeacherByWorKId(Teacher teacher);

	Teacher getTeacherByName(String teaherName);
}
