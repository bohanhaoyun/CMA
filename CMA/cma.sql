/*
Navicat MySQL Data Transfer

Source Server         : cma
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : cma

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-03-14 12:40:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for academe
-- ----------------------------
DROP TABLE IF EXISTS `academe`;
CREATE TABLE `academe` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `academeName` varchar(255) NOT NULL COMMENT '学院名称',
  `academeCode` varchar(255) DEFAULT NULL COMMENT '学院代码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of academe
-- ----------------------------
INSERT INTO `academe` VALUES ('1', '信息学院', '3081');
INSERT INTO `academe` VALUES ('2', '农学院', '3011');

-- ----------------------------
-- Table structure for classgroup
-- ----------------------------
DROP TABLE IF EXISTS `classgroup`;
CREATE TABLE `classgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级唯一标识',
  `session` varchar(255) DEFAULT NULL COMMENT '年级',
  `major` varchar(255) DEFAULT NULL COMMENT '专业名称',
  `name` varchar(255) DEFAULT NULL COMMENT '班级名称',
  `number` int(11) DEFAULT NULL COMMENT '班级人数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classgroup
-- ----------------------------
INSERT INTO `classgroup` VALUES ('1', '14', '信管', '14信管3班', '31');
INSERT INTO `classgroup` VALUES ('2', '14', '信管', '14信管2班', '31');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `courseId` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程唯一标识',
  `courseName` varchar(255) NOT NULL COMMENT '课程名称',
  `academe` varchar(255) NOT NULL COMMENT '院系',
  PRIMARY KEY (`courseId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '信息管理', '信息学院');
INSERT INTO `course` VALUES ('2', '计算机', '信息学院');
INSERT INTO `course` VALUES ('3', '畜牧学', '农学院');
INSERT INTO `course` VALUES ('4', '课程1', '学院1');
INSERT INTO `course` VALUES ('5', '课程2', '学院1');
INSERT INTO `course` VALUES ('6', '课程3', '学院2');
INSERT INTO `course` VALUES ('7', '课程4', '学院2');

-- ----------------------------
-- Table structure for coursedocument
-- ----------------------------
DROP TABLE IF EXISTS `coursedocument`;
CREATE TABLE `coursedocument` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `courseId` int(11) NOT NULL COMMENT '课程id',
  `teacherId` int(11) NOT NULL COMMENT '教师id',
  `classId` int(11) NOT NULL COMMENT '班级id',
  `documentType` varchar(255) NOT NULL COMMENT '课程材料类型',
  `period` varchar(255) NOT NULL COMMENT '授课学期',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `receivable` varchar(255) DEFAULT NULL COMMENT '应收份数',
  `received` varchar(255) DEFAULT NULL COMMENT '已收份数',
  `status` varchar(255) NOT NULL COMMENT '状态',
  `person` varchar(255) DEFAULT NULL COMMENT '处理人',
  `closingDate` datetime DEFAULT NULL COMMENT '截止日期',
  `finishDate` datetime DEFAULT NULL COMMENT '完成日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coursedocument
-- ----------------------------
INSERT INTO `coursedocument` VALUES ('1', '1', '1', '1', '试卷1', '2017-1018', '111', '31', '31', '已审核', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('2', '2', '2', '1', '试卷1', '2017-1018', '111', '31', '31', '未提交', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('3', '2', '2', '1', '试卷1', '2017-1018', '111', '31', '31', '未提交', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('4', '2', '3', '1', '试卷1', '2017-1018', '111', '31', '31', '未提交', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('5', '3', '3', '1', '试卷', '2017-1018', '111', '31', '31', '未提交', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('6', '2', '3', '1', '试卷', '2017-1018', '111', '31', '31', '未提交', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('7', '3', '1', '1', '试卷', '2017-1018', '111', '31', '31', '已审核', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('8', '3', '2', '1', '试卷', '2017-1018', '111', '31', '31', '未提交', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('9', '3', '1', '1', '试卷', '2017-1018', '111', '31', '31', '已审核', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('10', '3', '1', '1', '试卷', '2017-1018', '111', '31', '31', '已审核', '1', '2018-03-22 09:50:31', '2018-03-02 09:50:36');
INSERT INTO `coursedocument` VALUES ('11', '3', '1', '1', '其他', '2017-2018', '111', '31', '31', '已审核', '1', '2018-03-06 00:39:04', '2018-03-06 00:39:08');
INSERT INTO `coursedocument` VALUES ('12', '1', '1', '1', '试卷', '1', '1', null, null, '已审核', null, null, null);
INSERT INTO `coursedocument` VALUES ('13', '2', '1', '1', '材料', '11', '11111111111111111', null, null, '审核中', null, null, null);
INSERT INTO `coursedocument` VALUES ('14', '1', '1', '2', '试卷', '1', '23333323', null, null, '审核中', null, null, null);
INSERT INTO `coursedocument` VALUES ('15', '1', '1', '1', '试卷', '323', '2333232', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('16', '1', '1', '1', '试卷', '2123', '2313213131', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('17', '1', '1', '1', '试卷', '2', '23323', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('18', '1', '1', '1', '试卷', 'dda', 'dadad', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('19', '1', '1', '1', '试卷', 'dadadad', 'dada', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('20', '1', '1', '1', '试卷', 'da', 'dadadada', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('21', '1', '1', '1', '试卷', 'ad', 'addadadad', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('22', '1', '1', '1', '试卷', 'ad', 'addadadad', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('23', '1', '1', '1', '试卷', 'ddd', 'ddddd', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('24', '1', '2', '1', '试卷', '1', '1233', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('25', '1', '1', '1', '试卷', '33', '33333333', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('26', '2', '2', '1', '试卷', '顶顶顶', '顶顶顶', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('27', '1', '2', '2', '材料', 'd\'s\'d ', '多少多少', null, null, '未提交', null, null, null);
INSERT INTO `coursedocument` VALUES ('28', '1', '2', '1', '试卷', 'd', '的', null, null, '未提交', null, null, null);

-- ----------------------------
-- Table structure for courseteacher
-- ----------------------------
DROP TABLE IF EXISTS `courseteacher`;
CREATE TABLE `courseteacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '选课唯一标识',
  `teacherId` int(11) NOT NULL COMMENT '教师id',
  `courseId` int(11) NOT NULL COMMENT '课程id',
  `classId` int(11) NOT NULL COMMENT '班级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of courseteacher
-- ----------------------------

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `teacherId` int(11) NOT NULL AUTO_INCREMENT COMMENT '教师唯一标识',
  `workId` varchar(255) NOT NULL COMMENT '工号',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `password` varchar(255) NOT NULL COMMENT '登陆密码',
  `academe` varchar(255) NOT NULL COMMENT '学院',
  `imgUrl` varchar(255) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`teacherId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('1', '001', '教师1', '1111', '信息学院', null);
INSERT INTO `teacher` VALUES ('2', '002', '教师2', '002', '信息学院', null);
INSERT INTO `teacher` VALUES ('3', '003', '教师3', '003', '信息学院', null);
INSERT INTO `teacher` VALUES ('4', '004', '教师4', '004', '信息学院', null);
INSERT INTO `teacher` VALUES ('5', '005', '教师5', '005', '信息学院', null);
INSERT INTO `teacher` VALUES ('6', '006', '教师6', '006', '农学院', null);
INSERT INTO `teacher` VALUES ('7', '007', '教师7', '007', '信息学院', null);
INSERT INTO `teacher` VALUES ('8', '008', '教师8', '008', 'xx学院', null);
INSERT INTO `teacher` VALUES ('9', '009', '教师9', '009', '呵呵学院', null);
INSERT INTO `teacher` VALUES ('10', '010', '教师10', '010', '咳咳学院', null);
INSERT INTO `teacher` VALUES ('12', '20.0', '教师20', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('13', '20.0', '教师21', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('14', '20.0', '教师22', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('15', '20.0', '教师23', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('16', '20.0', '教师24', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('17', '20.0', '教师25', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('18', '20.0', '教师26', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('19', '20.0', '教师27', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('20', '20.0', '教师28', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('21', '20.0', '教师29', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('23', '20.0', '教师31', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('24', '20.0', '教师32', '222.0', '数学与信息学院', null);
INSERT INTO `teacher` VALUES ('25', '20.0', '教师33', '222.0', '数学与信息学院', null);

-- ----------------------------
-- Table structure for xxgl_je_headers
-- ----------------------------
DROP TABLE IF EXISTS `xxgl_je_headers`;
CREATE TABLE `xxgl_je_headers` (
  `JOURNAL_HEADER_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '表ID，主键，供其他表做外键',
  `JE_HEADER_ID` bigint(20) NOT NULL COMMENT '日记账头ID',
  `JOURNAL_NAME` varchar(100) DEFAULT NULL COMMENT '日记账名',
  `LEDGER_ID` bigint(20) DEFAULT NULL COMMENT '总账ID',
  `LEDGER_NAME` varchar(100) DEFAULT NULL COMMENT '总账名',
  `JE_CATEGORY` varchar(30) DEFAULT NULL COMMENT '日记账类别',
  `JE_SOURCE` varchar(30) DEFAULT NULL COMMENT '日记账来源',
  `PERIOD_NAME` varchar(15) DEFAULT NULL COMMENT '期间',
  `CURRENCY_CODE` varchar(15) DEFAULT NULL COMMENT '币种',
  `STATUS` varchar(1) NOT NULL COMMENT 'U为unposted，P为posted',
  `DATE_CREATED` varchar(30) DEFAULT NULL,
  `ACCRUAL_REV_FLAG` varchar(1) DEFAULT NULL,
  `MULTI_BAL_SEG_FLAG` varchar(1) DEFAULT NULL,
  `ACTUAL_FLAG` varchar(1) DEFAULT NULL,
  `CONVERSION_FLAG` varchar(1) DEFAULT NULL,
  `ACCOUNTING_DATE` varchar(30) NOT NULL COMMENT '会计日期',
  `BUDGET_VERSION_ID` bigint(20) DEFAULT NULL,
  `BALANCED_JE_FLAG` varchar(1) DEFAULT NULL,
  `BALANCING_SEGMENT_VALUE` varchar(25) DEFAULT NULL,
  `JE_BATCH_ID` bigint(20) NOT NULL,
  `BATCH_NAME` varchar(100) DEFAULT NULL COMMENT '批次名',
  `FROM_RECURRING_HEADER_ID` bigint(20) DEFAULT NULL,
  `EARLIEST_POSTABLE_DATE` varchar(30) DEFAULT NULL,
  `POSTED_DATE` varchar(30) DEFAULT NULL,
  `ACCRUAL_REV_EFFECTIVE_DATE` varchar(30) DEFAULT NULL,
  `ACCRUAL_REV_PERIOD_NAME` varchar(15) DEFAULT NULL,
  `ACCRUAL_REV_STATUS` varchar(1) DEFAULT NULL,
  `ACCRUAL_REV_JE_HEADER_ID` bigint(20) DEFAULT NULL,
  `ACCRUAL_REV_CHANGE_SIGN_FLAG` varchar(1) DEFAULT NULL,
  `DESCRIPTION` varchar(240) DEFAULT NULL,
  `CONTROL_TOTAL` double DEFAULT NULL,
  `RUNNING_TOTAL_DR` double DEFAULT NULL,
  `RUNNING_TOTAL_CR` double DEFAULT NULL,
  `RUNNING_TOTAL_ACCOUNTED_DR` double DEFAULT NULL,
  `CURRENCY_CONVERSION_RATE` double DEFAULT NULL,
  `CURRENCY_CONVERSION_TYPE` varchar(30) DEFAULT NULL,
  `CURRENCY_CONVERSION_DATE` varchar(30) DEFAULT NULL,
  `DOC_SEQUENCE_ID` bigint(20) DEFAULT NULL,
  `DOC_SEQUENCE_VALUE` double DEFAULT NULL,
  `PARENT_JE_HEADER_ID` bigint(20) DEFAULT NULL,
  `REVERSED_JE_HEADER_ID` bigint(20) DEFAULT NULL,
  `MULTI_CURRENCY_FLAG` varchar(1) DEFAULT NULL,
  `RUNNING_TOTAL_ACCOUNTED_CR` double DEFAULT NULL,
  `ERP_CREATION_DATE` varchar(30) DEFAULT NULL,
  `ERP_LAST_UPDATE_DATE` varchar(30) DEFAULT NULL,
  `OBJECT_VERSION_NUMBER` bigint(20) NOT NULL DEFAULT '1' COMMENT '行版本号，用来处理锁',
  `CREATION_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) NOT NULL DEFAULT '-1',
  `LAST_UPDATED_BY` int(11) NOT NULL DEFAULT '-1',
  `LAST_UPDATE_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_LOGIN` int(11) DEFAULT NULL,
  `PROGRAM_APPLICATION_ID` int(11) DEFAULT NULL,
  `PROGRAM_ID` int(11) DEFAULT NULL,
  `PROGRAM_UPDATE_DATE` date DEFAULT NULL,
  `REQUEST_ID` int(11) DEFAULT NULL,
  `ATTRIBUTE_CATEGORY` varchar(30) DEFAULT NULL,
  `ATTRIBUTE1` varchar(240) DEFAULT NULL,
  `ATTRIBUTE2` varchar(240) DEFAULT NULL,
  `ATTRIBUTE3` varchar(240) DEFAULT NULL,
  `ATTRIBUTE4` varchar(240) DEFAULT NULL,
  `ATTRIBUTE5` varchar(240) DEFAULT NULL,
  `ATTRIBUTE6` varchar(240) DEFAULT NULL,
  `ATTRIBUTE7` varchar(240) DEFAULT NULL,
  `ATTRIBUTE8` varchar(240) DEFAULT NULL,
  `ATTRIBUTE9` varchar(240) DEFAULT NULL,
  `ATTRIBUTE10` varchar(240) DEFAULT NULL,
  `ATTRIBUTE11` varchar(240) DEFAULT NULL,
  `ATTRIBUTE12` varchar(240) DEFAULT NULL,
  `ATTRIBUTE13` varchar(240) DEFAULT NULL,
  `ATTRIBUTE14` varchar(240) DEFAULT NULL,
  `ATTRIBUTE15` varchar(240) DEFAULT NULL,
  PRIMARY KEY (`JOURNAL_HEADER_ID`),
  UNIQUE KEY `XXGL_JE_HEADERS_U1` (`JOURNAL_HEADER_ID`),
  KEY `XXGL_JE_HEADERS_N1` (`JE_HEADER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xxgl_je_headers
-- ----------------------------
