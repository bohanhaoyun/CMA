package priv.scau.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import priv.scau.dao.DepartmentMapper;
import priv.scau.en.Department;
import priv.scau.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired
	DepartmentMapper mapper;
	
	@Override
	public List<Department> getDepartmentByAcademe(Department de) {
		// TODO Auto-generated method stub
		return mapper.getDepartmentByAcademe(de);
	}

	@Override
	public void addDepartment(Department dto) {
		// TODO Auto-generated method stub
		mapper.insertSelective(dto);
	}

	@Override
	public void delDepartment(Department dto) {
		// TODO Auto-generated method stub
		mapper.deleteByPrimaryKey(dto.getId());
	}

	@Override
	public List<Department> getDepartByUser(Department de) {
		// TODO Auto-generated method stub
		return mapper.getDepartByUser(de);
	}

}
