package priv.scau.en;

import java.util.Date;

import javax.persistence.Transient;

public class Coursedocument {
    private Integer id;

    private Integer courseid;

    private Integer teacherid;

    private Integer classid;

    private String documenttype;

    private String period;

    private String remark;

    private String receivable;

    private String received;

    private String status;

    private String person;

    private Date closingdate;

    private Date finishdate;

    @Transient
    private String teaherName;
    
    @Transient
    private String workId;
    
    @Transient
    private String courseName;
    
    @Transient
    private String className;
    
    @Transient
    private String key;
    
    @Transient
    private String alCount;
    
    @Transient
    private String unCount;
    
    

	public String getAlCount() {
		return alCount;
	}

	public void setAlCount(String alCount) {
		this.alCount = alCount;
	}

	public String getUnCount() {
		return unCount;
	}

	public void setUnCount(String unCount) {
		this.unCount = unCount;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getTeaherName() {
		return teaherName;
	}

	public void setTeaherName(String teaherName) {
		this.teaherName = teaherName;
	}

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public String getDocumenttype() {
        return documenttype;
    }

    public void setDocumenttype(String documenttype) {
        this.documenttype = documenttype;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReceivable() {
        return receivable;
    }

    public void setReceivable(String receivable) {
        this.receivable = receivable;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public Date getClosingdate() {
        return closingdate;
    }

    public void setClosingdate(Date closingdate) {
        this.closingdate = closingdate;
    }

    public Date getFinishdate() {
        return finishdate;
    }

    public void setFinishdate(Date finishdate) {
        this.finishdate = finishdate;
    }

	@Override
	public String toString() {
		return "Coursedocument [id=" + id + ", courseid=" + courseid + ", teacherid=" + teacherid + ", classid="
				+ classid + ", documenttype=" + documenttype + ", period=" + period + ", remark=" + remark
				+ ", receivable=" + receivable + ", received=" + received + ", status=" + status + ", person=" + person
				+ ", closingdate=" + closingdate + ", finishdate=" + finishdate + ", teaherName=" + teaherName
				+ ", workId=" + workId + ", courseName=" + courseName + ", className=" + className + ", key=" + key
				+ ", alCount=" + alCount + ", unCount=" + unCount + "]";
	}
}