package priv.scau.dao;

import java.util.List;

import priv.scau.en.Classgroup;

public interface ClassgroupMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Classgroup record);

    int insertSelective(Classgroup record);

    Classgroup selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Classgroup record);

    int updateByPrimaryKey(Classgroup record);
    
    List<Classgroup> selectAllClass();

	Classgroup getCoursePerson(Classgroup clas);

	Classgroup getClassByName(String className);
}