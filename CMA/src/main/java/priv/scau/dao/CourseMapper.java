package priv.scau.dao;

import java.util.List;

import priv.scau.en.Course;

public interface CourseMapper {
    int deleteByPrimaryKey(Integer courseid);

    int insert(Course record);

    int insertSelective(Course record);

    Course selectByPrimaryKey(Integer courseid);

    int updateByPrimaryKeySelective(Course record);

    int updateByPrimaryKey(Course record);
    
    List<Course> selectAllCourse(Course course);

	Course getCourseByName(String courseName);
}