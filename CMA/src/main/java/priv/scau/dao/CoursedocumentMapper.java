package priv.scau.dao;

import java.util.List;
import java.util.Map;

import priv.scau.en.Coursedocument;
import priv.scau.en.LineCharts;

public interface CoursedocumentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Coursedocument record);

    int insertSelective(Coursedocument record);

    Coursedocument selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Coursedocument record);

    int updateByPrimaryKey(Coursedocument record);
    
    List<Coursedocument> getAllDocuments(Coursedocument doc);

	List<Coursedocument> getDocumentByWorkId(Coursedocument course);

	void changeStatus(Coursedocument document);

	List<Coursedocument> getDocumentApproval(Coursedocument course);

	List<Coursedocument> getDocumentBySearch(Coursedocument document);

	List<Coursedocument> getCountByCourse(Coursedocument document);

	List<LineCharts> getAcademeDocument(Coursedocument document);
	
	List<Coursedocument> getPeried();

	List<Coursedocument> getAlSubmitDocument(Coursedocument course);

	List<Coursedocument> getDocumentBySearchSessionTeacher(Coursedocument document);

	List<Coursedocument> getDocumentByDepartment(Map<String, Object> map);

	List<Coursedocument> getDocumentByAcademe(Map<String, Object> map);
}