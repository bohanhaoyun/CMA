package priv.scau.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import priv.scau.dao.AcademeMapper;
import priv.scau.en.Academe;
import priv.scau.service.AcademeService;
@Service
public class AcademeServiceImpl  implements AcademeService{
	
	@Autowired
	AcademeMapper acMapper;
	
	@Override
	public List<Academe> getAcademeGrid(String key) {
		// TODO 自动生成的方法存根
		return acMapper.getAcademeGrid(key);
	}

	@Override
	public List<Academe> checkCode(Academe academe) {
		// TODO 自动生成的方法存根
		return acMapper.checkCode(academe);
	}

	@Override
	public void addAcademe(Academe academe) {
		// TODO 自动生成的方法存根
		acMapper.insertSelective(academe);
	}

	@Override
	public void updateAcademe(Academe academe) {
		// TODO 自动生成的方法存根
		acMapper.updateByPrimaryKeySelective(academe);
	}

	@Override
	public void delAcademe(Academe academe) {
		// TODO Auto-generated method stub
		acMapper.deleteByPrimaryKey(academe.getId());
	}

	@Override
	public Academe getAcademeByName(String academeName) {
		// TODO Auto-generated method stub
		return acMapper.getAcademeByName(academeName);
	}

}
