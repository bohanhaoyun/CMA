package priv.scau.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import priv.scau.en.Coursedocument;
import priv.scau.en.Teacher;
import priv.scau.service.ClassgroupService;
import priv.scau.service.CourseService;
import priv.scau.service.CoursedocumentService;
import priv.scau.service.ShiroService;

@Controller
public class CoursedocumentController {
	
	@Autowired
	CoursedocumentService documentService;
	
	@Autowired
	ShiroService shiroService;
	
	@Autowired
	ClassgroupService classService;
	/**
	 * 获取课程材料列表
	 * @param page	分页数据页码
	 * @param limit	分页数据每页的行数
	 * @param key	用于搜索的关键字
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/courseDocument/getDocument",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDocument(Integer page,Integer limit,String key,
			HttpServletRequest request,HttpServletResponse response){
		System.out.println(page+"================"+limit);
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		PageHelper.startPage(page, limit);
		List<Coursedocument> list = documentService.getDocument(key, page, limit);
		PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", 0);
		return resultMap;
	}
	
	/**
	 * 新增课程材料
	 * @param dto
	 * @return
	 */
	@RequestMapping(value="/courseDocument/addNewDocument",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addNewDocument(Coursedocument dto,HttpServletResponse response,HttpServletRequest request){
		//System.out.println(key);
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		dto.setStatus("未提交");
		dto.setReceived("0");
		dto.setReceivable(classService.getCoursePerson(dto));
		documentService.addNewDocument(dto);
		resultMap.put("msg","success");
		return resultMap;
	}
	
	/**
	 * 根据不同教师获取对应的应交课程材料
	 * @param page	分页数据页码
	 * @param limit	分页数据每页的行数
	 * @param key	用于搜索的关键字
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/courseDocument/getDocumentByTeacher",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDocumentByTeacher(Integer page,Integer limit,String key,
			HttpServletRequest request,HttpServletResponse response){
		//System.out.println(page+"================"+limit);
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		PageHelper.startPage(page, limit);
		Coursedocument course = new Coursedocument();
		course.setKey(key);
		course.setTeacherid(teacher.getTeacherid());
		List<Coursedocument> list = documentService.getDocumentByWorkId(course);
		PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", 0);
		return resultMap;
	}
	
	
	@RequestMapping(value="/courseDocument/getAlSubmitDocument",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getAlSubmitDocument(Integer page,Integer limit,String key,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		PageHelper.startPage(page, limit);
		Coursedocument course = new Coursedocument();
		course.setKey(key);
		course.setTeacherid(teacher.getTeacherid());
		List<Coursedocument> list = documentService.getAlSubmitDocument(course);
		PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", 0);
		return resultMap;
	}
	/**
	 * 获取审批中的课程材料
	 * @param page	分页数据页码
	 * @param limit	分页数据每页的行数
	 * @param key	用于搜索的关键字
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/courseDocument/getDocumentApproval",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDocumentApproval(Integer page,Integer limit,String key,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		//System.out.print(page+"ss"+limit);
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		PageHelper.startPage(page, limit);
		Coursedocument course = new Coursedocument();
		course.setKey(key);
		course.setTeacherid(teacher.getTeacherid());
		List<Coursedocument> list = documentService.getDocumentApproval(course);
		PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", 0);
		return resultMap;
	}
	
	/**
	 * 更改课程材料提交状态（未提交->审核中，审核中->已审核）
	 * @param id	课程材料id
	 * @param oldStatus		课程材料状态
	 * @return
	 */
	@RequestMapping(value="/courseDocument/changeStatus",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> changeStatus(Integer id,String oldStatus){
		//System.out.println(page+"================"+limit);
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Coursedocument document = new Coursedocument();
		//System.out.println(id);
		document.setId(id);
		document.setStatus(oldStatus);
		try {
			documentService.changeStatus(document);
			resultMap.put("msg","success");
		}catch(Exception e) {
			resultMap.put("msg",e.getMessage());
			e.printStackTrace();
		}
		
		return resultMap;
	}
	
	@RequestMapping(value="/courseDocument/getDocumentBySearch",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDocumentBySearch(Coursedocument doc,Integer page,Integer limit,
			HttpServletRequest request,HttpServletResponse response){
		//System.out.println(doc);
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		List<Coursedocument> list = new ArrayList<Coursedocument>();
		try {
			list = documentService.getDocumentBySearch(doc,page,limit);
			PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
			resultMap.put("data",pageInfo.getList());
			resultMap.put("msg","success");
			resultMap.put("count", pageInfo.getTotal());
			resultMap.put("code", 0);
		}catch(Exception e) {
			resultMap.put("msg",e.getMessage());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value="/courseDocument/getDocumentByDepartment",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDocumentByDepartment(String period,String department,Integer page,Integer limit,
			HttpServletRequest request,HttpServletResponse response){
		System.out.println("ss:"+period+"LLL:"+department);
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		List<Coursedocument> list = new ArrayList<Coursedocument>();
		try {
			list = documentService.getDocumentByDepartment(period,department,page,limit);
			PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
			resultMap.put("data",pageInfo.getList());
			resultMap.put("msg","success");
			resultMap.put("count", pageInfo.getTotal());
			resultMap.put("code", 0);
		}catch(Exception e) {
			resultMap.put("msg",e.getMessage());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value="/courseDocument/getCountByCourse",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getCountByCourse(Coursedocument doc,Integer page,Integer limit,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		List<Coursedocument> list = new ArrayList<Coursedocument>();
		try {
			list = documentService.getCountByCourse(doc);
			PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
			resultMap.put("data",pageInfo.getList());
			resultMap.put("msg","success");
			resultMap.put("count", pageInfo.getTotal());
			resultMap.put("code", 0);
		}catch(Exception e) {
			resultMap.put("msg",e.getMessage());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value="/courseDocument/getAcademeDocument",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getAcademeDocument(String status,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		List<Coursedocument> list = new ArrayList<Coursedocument>();
		Coursedocument document = new Coursedocument();
		System.out.print("ssss"+status);
		document.setStatus(status);
		try {
			resultMap = documentService.getAcademeDocument(document);
		}catch(Exception e) {
			resultMap.put("msg",e.getMessage());
			e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value="/courseDocument/getPeriod",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getPeriod(HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		List<Coursedocument> list = new ArrayList<Coursedocument>();
		try {
			list = documentService.getPeriod();
			resultMap.put("result", "success");
			resultMap.put("data", list);
		}catch(Exception e) {
			resultMap.put("result", "fail");
			resultMap.put("msg",e.getMessage());
			//e.printStackTrace();
		}
		return resultMap;
	}
	
	@RequestMapping(value="/courseDocument/getDocumentByAcademe",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDocumentByAcademe(Integer page,Integer limit,String academeName,String status,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		PageHelper.startPage(page, limit);
		List<Coursedocument> list = documentService.getDocumentByAcademe(academeName,status, page, limit);
		PageInfo<Coursedocument> pageInfo = new PageInfo<Coursedocument>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", 0);
		return resultMap;
	}
	
	//通过excel导入教师信息
		@RequestMapping(value="/courseDocument/uploadDocument",method=RequestMethod.POST)  
	    @ResponseBody  
	    public Map<String, Object> uploadDocument(MultipartFile file,HttpServletRequest request){  
			Map<String, Object> resultMap = new HashMap<String ,Object>();
	        String fileName = file.getOriginalFilename();    
	        //System.out.println(fileName);
	        XSSFWorkbook workBook = null;
			try {
				InputStream input = file.getInputStream();  
				workBook = new XSSFWorkbook(input);
			} catch (IOException e) {
				resultMap.put("result", "success");
				resultMap.put("msg", "文件解析失败");
				e.printStackTrace();
				return resultMap;
			}  
	        XSSFSheet sheet = workBook.getSheetAt(0);
	        List<Coursedocument> list = new ArrayList<Coursedocument>();
	        for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
	        	Coursedocument doc = new Coursedocument();
	        	XSSFRow row = sheet.getRow(i);
	        	doc.setCourseName(row.getCell(0).toString());
	        	doc.setTeaherName(row.getCell(1).toString());
	        	doc.setClassName(row.getCell(2).toString());
	        	doc.setDocumenttype(row.getCell(3).toString());
	        	doc.setPeriod(row.getCell(4).toString());
	        	doc.setRemark(row.getCell(5).toString());
	        	list.add(doc);
	        }
	        
	        String re = documentService.batchInsert(list);
	        
	        resultMap.put("result", "success");
	        resultMap.put("msg", "文件上传成功");
	        return resultMap;  
	    }  
}
