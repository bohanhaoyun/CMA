package priv.scau.dao;

import java.util.List;

import priv.scau.en.Department;

public interface DepartmentMapper {
    int deleteByPrimaryKey(Integer id);

	int insert(Department record);

	int insertSelective(Department record);

	Department selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Department record);

	int updateByPrimaryKey(Department record);

	List<Department> getDepartmentByAcademe(Department de);

	List<Department> getDepartByUser(Department de);
}