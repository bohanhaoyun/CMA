package priv.scau.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import priv.scau.en.Academe;
import priv.scau.service.AcademeService;
import priv.scau.service.ShiroService;
@Controller
public class AcademeController {
	
	@Autowired
	AcademeService academeService;
	
	
	/**
	 * 加载学院表格，通过增加空白列实现新增表格方法
	 * @param page	分页数据页码
	 * @param limit	分页数据每页的行数
	 * @param key	用于搜索的关键字
	 * @param request	
	 * @param response
	 * @return	
	 */
	@RequestMapping(value="/academe/getAcademeGrid",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getAcademeGrid(Integer page,Integer limit,String key,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		PageHelper.startPage(page, limit);
		//System.out.println("key====="+key);
		List<Academe> list = new ArrayList<Academe>();
		if(key != null && "add".equals(key)) {
			//增加空白列
			key = null;
			list = academeService.getAcademeGrid(key);
			Academe newAcademe = new Academe();
			list.add(0, newAcademe);
		}else {
			list = academeService.getAcademeGrid(key);
		}
		
		PageInfo<Academe> pageInfo = new PageInfo<Academe>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		resultMap.put("code", 0);
		return resultMap;
	}
	
	
	/**
	 * 增加学院，存在则更新，不存在则插入
	 * @param academe
	 * @return
	 */
	@RequestMapping(value="/academe/addAcademe",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addAcademe(Academe academe,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		//验证代码是否唯一
		List<Academe> checkList = new ArrayList<Academe>();
		checkList = academeService.checkCode(academe);
		if(checkList.size()>0) {
			resultMap.put("msg","学院代码已存在");
		}else {
			if(academe.getId() != null) {
				academeService.updateAcademe(academe);	
			}else {
				academeService.addAcademe(academe);										
			}
			resultMap.put("msg","插入成功");
		}
		return resultMap;
	}
	
	@RequestMapping(value="/academe/delAcademe",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delAcademe(Academe academe){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		//验证代码是否唯一
		academeService.delAcademe(academe);
		resultMap.put("msg", "删除成功");
		return resultMap;
	}
}
