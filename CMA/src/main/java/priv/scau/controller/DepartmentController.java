package priv.scau.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import priv.scau.en.Academe;
import priv.scau.en.Department;
import priv.scau.en.Teacher;
import priv.scau.service.DepartmentService;
import priv.scau.service.ShiroService;

@Controller
public class DepartmentController {
	
	@Autowired
	DepartmentService service;
	
	@Autowired
	ShiroService shiroService;
	
	@RequestMapping(value="/department/getDepartmentByAcademe",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDepartmentByAcademe(Integer page,Integer limit,String academe,String key,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		PageHelper.startPage(page, limit);
		Department de = new Department();
		de.setAcademe(academe);
		List<Department> list = new ArrayList<Department>();
		list = service.getDepartmentByAcademe(de);
		if("add".equals(key)) {
			Department department = new Department();
			department.setAcademe(academe);
			list.add(0,department);
		}
		PageInfo<Department> pageInfo = new PageInfo<Department>(list);
		resultMap.put("data",pageInfo.getList());
		resultMap.put("msg","success");
		resultMap.put("count", pageInfo.getTotal());
		
		resultMap.put("code", 0);
		return resultMap;
	}
	
	
	@RequestMapping(value="/department/addDepartment",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addDepartment(Department dto,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		service.addDepartment(dto);
		resultMap.put("msg","success");
		return resultMap;
	}
	
	@RequestMapping(value="/department/delDepartment",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delDepartment(Department dto,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		//System.out.println(dto);
		service.delDepartment(dto);
		resultMap.put("msg","success");
		return resultMap;
	}
	
	@RequestMapping(value="/department/getDepartByUser",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDepartByUser(){
		Map<String, Object> resultMap = new HashMap<String ,Object>();
		Map<String, Object> userMap = new HashMap<String,Object>();
		userMap = shiroService.getUserMsg();
		Teacher teacher = new Teacher();
		teacher = (Teacher) userMap.get("user");
		Department de = new Department();
		de.setAcademe(teacher.getAcademe());
		List<Department> list = new ArrayList<Department>();
		list = service.getDepartByUser(de);
		resultMap.put("data", list);
		resultMap.put("msg","success");
		return resultMap;
	}
}
